<?php

namespace App\Command;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

/**
 * Class ImportUsers
 * @package App\Command
 */
class ImportUsers extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:import-users';

    /**
     * @var array
     */
    protected static array $companies = [];

    /**
     * @var string
     */
    protected static string $usersApi = 'https://jsonplaceholder.typicode.com/users';

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * ImportUsers constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Import Users from API')
            ->setHelp('This command allows you to import users from the given API...');
    }

    /**
     * @param string $name
     * @return Company|null
     */
    protected function getCompany(string $name): ?Company
    {
        $cache = $this->getCompanyCache($name);

        if (isset($cache)) {
            return $cache;
        }

        $companyRepository = $this->entityManager->getRepository(Company::class);
        /** @var Company $company */
        $company = $companyRepository->findOneBy(compact('name'));

        if (isset($company)) {
            $this->setCompanyCache($name, $company);
        }

        return $this->getCompanyCache($name);
    }

    /**
     * @param string $name
     * @return Company|null
     */
    protected function getCompanyCache(string $name): ?Company
    {
        if (isset(self::$companies[$name])) {
            return self::$companies[$name];
        }

        return null;
    }

    /**
     * @param string $key
     * @param Company $company
     * @return Company
     */
    protected function setCompanyCache(string $key, Company $company): Company
    {
        self::$companies[$key] = $company;
        return $company;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $result = (new CLient)->request(
                'GET',
                self::$usersApi
            );
            $users = json_decode($result->getBody()->getContents());

            foreach ($users as $user) {
                $userEntity = new User;
                $userEntity->setName($user->name);
                $userEntity->setUsername($user->username);
                $userEntity->setEmail($user->email);
                $userEntity->setAddressStreet($user->address->street);
                $userEntity->setAddressSuite($user->address->suite);
                $userEntity->setAddressCity($user->address->city);
                $userEntity->setAddressZipcode($user->address->zipcode);
                $userEntity->setAddressGeoLat($user->address->geo->lat);
                $userEntity->setAddressGeoLng($user->address->geo->lng);
                $userEntity->setPhone($user->phone);
                $userEntity->setWebsite($user->website);

                if (isset($user->company->name)) {
                    $company = $this->getCompany($user->company->name);

                    if (!$company) {
                        $companyEntity = new Company;
                        $companyEntity->setName($user->company->name);
                        $companyEntity->setBs($user->company->bs);
                        $companyEntity->setCatchPharse($user->company->catchPhrase);
                        $userEntity->setCompany(
                            $this->setCompanyCache($user->company->name, $companyEntity)
                        );
                        $this->entityManager->persist($companyEntity);
                    }
                }

                $this->entityManager->persist($userEntity);
            }

            $this->entityManager->flush();
        } catch (GuzzleException $e) {
            echo $e->getMessage();
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}