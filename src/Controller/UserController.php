<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{
    /**
     * @Route("/users/{id}", name="user")
     * @param int $id
     * @return Response
     */
    public function index(int $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(compact('id'));

        if (!isset($user)) {
            throw $this->createNotFoundException('The User does not exist');
        }

        return $this->json(
            [
                'data' => $user
            ]
        );
    }
}
