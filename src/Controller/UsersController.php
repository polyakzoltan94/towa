<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UsersController
 * @package App\Controller
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     * @return Response
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->json(
            [
                'data' => array_map(
                    function (User $user) {
                        return [
                            'id' => $user->getId(),
                            'name' => $user->getName(),
                            'username' => $user->getUsername(),
                        ];
                    },
                    $users
                )
            ]
        );
    }
}
