<?php
namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_suite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_zipcode;

    /**
     * @ORM\Column(type="float")
     */
    private $address_geo_lat;

    /**
     * @ORM\Column(type="float")
     */
    private $address_geo_lng;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $website;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddressStreet(): ?string
    {
        return $this->address_street;
    }

    public function setAddressStreet(string $address_street): self
    {
        $this->address_street = $address_street;

        return $this;
    }

    public function getAddressSuite(): ?string
    {
        return $this->address_suite;
    }

    public function setAddressSuite(string $address_suite): self
    {
        $this->address_suite = $address_suite;

        return $this;
    }

    public function getAddressCity(): ?string
    {
        return $this->address_city;
    }

    public function setAddressCity(string $address_city): self
    {
        $this->address_city = $address_city;

        return $this;
    }

    public function getAddressZipcode(): ?string
    {
        return $this->address_zipcode;
    }

    public function setAddressZipcode(string $address_zipcode): self
    {
        $this->address_zipcode = $address_zipcode;

        return $this;
    }

    public function getAddressGeoLat(): ?float
    {
        return $this->address_geo_lat;
    }

    public function setAddressGeoLat(float $address_geo_lat): self
    {
        $this->address_geo_lat = $address_geo_lat;

        return $this;
    }

    public function getAddressGeoLng(): ?float
    {
        return $this->address_geo_lng;
    }

    public function setAddressGeoLng(float $address_geo_lng): self
    {
        $this->address_geo_lng = $address_geo_lng;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'username' => $this->getUsername(),
            'email' => $this->getEmail(),
            'address' => [
                'street' => $this->getAddressStreet(),
                'suite' => $this->getAddressSuite(),
                'city' => $this->getAddressCity(),
                'zipcode' => $this->getAddressZipcode(),
                'geo' => [
                    'lat' => $this->getAddressGeoLat(),
                    'lng' => $this->getAddressGeoLng(),
                ]
            ],
            'phone' => $this->getPhone(),
            'website' => $this->getWebsite(),
            'company' =>[
                'name' => $this->getCompany()->getName(),
                'catchPhrase' => $this->getCompany()->getCatchPharse(),
                'bs' => $this->getCompany()->getBs(),
            ],
        ];
    }
}
