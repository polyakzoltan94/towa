# Towa interview

> This product allows you to import from the `https://jsonplaceholder.typicode.com/users` API and import it on the otherhand it also serving two endpoint /users and users/{id}  

## Requirements

The following dependencies need to be installed to start the project:

- PHP >=7.2.5
- Postgres 13
- Composer 3+

## Installation

1.) `git clone https://gitlab.com/polyakzoltan94/towa`

2.) `composer install`

3.) `symfony check:requirements`

3.1.) Some PHP components might missing, install them with PECL or apt install

4.) `docker volume create postgres-data`

4.1.) `docker run -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=password -e POSTGRES_DB=towa -p 5432:5432 -v postgres-data:/var/lib/postgresql/data/ postgres:13 &`

5.) In the project root run: `php bin/console doctrine:migrations:migrate`

5.1.) You can seed the DB from the API by using: `php bin/console app:import-users`

6.) Start the app: `symfony server:start`

Use development port like: http://localhost:42099/

## Routes

### <localhost:port>/users
Return all users from the database

### <localhost:port>/users/{id}
Return a specific user from the database which comes from parameter

## Ideas

### CLI Importer
One timer script, and I used it because the low volume of data, no pagination/chunking needed.

### Internal API endpoints
I wanted to split the backend/frontend so I only send json response from all the endpoints.

### In-memory cache
When I'm reading the external users endpoint I wanted to make sure that companies must cached locally (custom business logic)

### Cors policy
I got some local issues regarding CORS so I search for I applied one.

